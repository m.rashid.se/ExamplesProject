package bzu.edu.examplesproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SimpleListViewActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list_view);


        if(getIntent() != null){
            Bundle extraParams =  getIntent().getExtras();
            String whichList =  extraParams.getString("whichList");

            if(whichList.equals("AndroidItem")){
                //******Populate List with android default item layout
                populateListWithAndroidItemLayout();
                registerClickCallback();
            }else if(whichList.equals("MyItem")){

                //******Populate List with My item layout
                populateListWithOurItemLayout();
                registerClickCallback();
            }

        }

    }



    private void populateListWithAndroidItemLayout() {
        //*** (Part 1) create list of Items (in our case it will be array)
        String[] cars = {"Suzuki Mehraan", "Honda City", "Honda Civic", "Rolls Royce", "Ferrari", "Audi A4", "Audi A5", "Audi A6", "Audi A8", "Audi Q3", "Audi Q"};


        //*** (Part 2) Build Adapter
        arrayAdapter = new ArrayAdapter<String>(
                this,                                   //context for the activity
                android.R.layout.simple_list_item_1,    //layout for items of the list, this layout is taken from android default layouts
                cars                                    //item data which is displayed on list, in our case it is array
        );

        //*** (Part 3) configure the list view and set adapter which we build
        listView = (ListView) findViewById(R.id.lstvSimpleListView);
        listView.setAdapter(arrayAdapter);

    }

    private void populateListWithOurItemLayout() {
        //*** (Part 1) create list of Items (in our case it will be array)
        String[] cars = {"Suzuki Mehraan", "Honda City", "Honda Civic", "Rolls Royce", "Ferrari", "Audi A4", "Audi A5", "Audi A6", "Audi A8", "Audi Q3", "Audi Q"};


        //*** (Part 2) Build Adapter
        arrayAdapter = new ArrayAdapter<String>(
                this,                                   //context for the activity
                R.layout.item_simple_list_2,    //layout for items of the list, this layout is taken from android default layouts
                cars                                    //item data which is displayed on list, in our case it is array
        );

        //*** (Part 3) configure the list view and set adapter which we build
        listView = (ListView) findViewById(R.id.lstvSimpleListView);
        listView.setAdapter(arrayAdapter);
    }

    private void registerClickCallback() {
        listView = (ListView)findViewById(R.id.lstvSimpleListView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View viewClicked, int position, long id) {
                TextView textView = (TextView) viewClicked;
                String msg = String.format("You clicked row# %s which contains %s car item",position, textView.getText());
                Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
            }
        });
    }
}
