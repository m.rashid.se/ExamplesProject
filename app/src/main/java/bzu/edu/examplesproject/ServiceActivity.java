package bzu.edu.examplesproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import bzu.edu.examplesproject.Services.ServiceOne;

public class ServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        Button btnStartService = (Button) findViewById(R.id.btnStartService);
        Button btnStopService = (Button) findViewById(R.id.btnStopService);

        btnStartService.setOnClickListener(onClickListener);
        btnStopService.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btnStartService:
                    startService();
                    break;
                case R.id.btnStopService:
                    stopService();
                    break;
            }
        }
    };

    private void startService(){
        Intent intent = new Intent(this, ServiceOne.class);
        startService(intent);
    }
    private void stopService(){
        Intent intent = new Intent(this, ServiceOne.class);
        stopService(intent);
    }
}
