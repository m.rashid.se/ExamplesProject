package bzu.edu.examplesproject.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by mrashid on 5/22/2016.
 */
public class WebRequestTask extends AsyncTask<String, Void, String> {

    ProgressDialog progressDialog;
    Context context;
    TextView textView;
    Button button;

    public WebRequestTask(Context context, TextView textView, Button button) {
        this.context = context;
        this.textView = textView;
        this.button = button;
    }

    // Show Progress bar before downloading Music
    @Override
    protected void onPreExecute() {
        // Shows Progress Bar Dialog and then call doInBackground method
        progressDialog = ProgressDialog.show(context, "", "Loading. Please wait...", true);
    }

    @Override
    protected String doInBackground(String... params) {
        InputStream inputStream = null;
        URL url = null;
        HttpURLConnection httpURLConnection = null;
        BufferedReader bufferedReader = null;
        try {
//            url = new URL("ftp://mirror.csclub.uwaterloo.ca/index.html");
            url = new URL(params[0]);
            URLConnection conn = url.openConnection();
            if(conn instanceof HttpURLConnection){
                //cast it into the HttpURLConnection
                httpURLConnection = (HttpURLConnection)conn;
                int responseCode = -1;

                //connect
                httpURLConnection.connect();
                responseCode = httpURLConnection.getResponseCode();
                if(responseCode == httpURLConnection.HTTP_OK){
                    inputStream = httpURLConnection.getInputStream();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder stringBuilder = new StringBuilder();
                    String strLine = null;
                    while ((strLine = bufferedReader.readLine()) != null) {
                        stringBuilder.append(strLine);
                    }
                    return stringBuilder.toString();
                }
            }
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {

            if(httpURLConnection != null)
                httpURLConnection.disconnect();
            try {
                if(bufferedReader != null){
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        //called on ui thread
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }

        System.out.println(result);
        Toast myToast = Toast.makeText(context, "Successfully loaded!", Toast.LENGTH_LONG);
        myToast.setGravity(Gravity.CENTER, 0, 0);
        myToast.show();

        textView.setText(result);
        button.setEnabled(true);
    }
}
