package bzu.edu.examplesproject.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by mrashid on 5/22/2016.
 */
public class MyTaskOne extends AsyncTask<Void,Integer,String> {

    Context context;
    ProgressDialog progressDialog;
    TextView textView;
    Button button;

    public MyTaskOne(Context context, TextView textView,Button button) {
        this.context = context;
        this.textView = textView;
        this.button = button;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Download in progress...");
        progressDialog.setMax(10);
        progressDialog.setProgress(0);
        progressDialog.setProgressStyle(progressDialog.STYLE_HORIZONTAL);
        progressDialog.show();

    }



    @Override
    protected String doInBackground(Void... voids) {
        String result;
        int i = 0;
        //synchronized means that only one thread at a time should be able to access
        synchronized (this){
            while (i<10) {
                try {
                    wait(1500);
                    i++;
                    publishProgress(i);//This function will invoked  onProgressUpdate()
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        result = "Download complete....";
        return result;
    }

    //(three dots, ...) called varargs (Arbitrary Number of Arguments)
    @Override
    protected void onProgressUpdate(Integer... values) {
        int progress = values[0];
        progressDialog.setProgress(progress);
        textView.setText("Download in progress...");
    }

    @Override
    protected void onPostExecute(String result) {
        button.setEnabled(true);
        textView.setText(result);
        progressDialog.hide();
    }
}
