package bzu.edu.examplesproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import bzu.edu.examplesproject.AsyncTasks.MyTaskOne;
import bzu.edu.examplesproject.AsyncTasks.WebRequestTask;

public class WebRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_request);

        final TextView txtvWebResponse = (TextView)findViewById(R.id.txtvWebResponse);
        final Button btnMakeRequest = (Button)findViewById(R.id.btnMakeRequest);
        btnMakeRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebRequestTask webRequestTask = new WebRequestTask(WebRequestActivity.this,txtvWebResponse,btnMakeRequest);
                String url = "http://62.210.249.204/kanvas-php/public/TestFile.txt";
                webRequestTask.execute(url);
                btnMakeRequest.setEnabled(false);
            }
        });
    }
}
