package bzu.edu.examplesproject.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Created by mrashid on 5/23/2016.
 * Service should always run on another thread not at UI thread
 */
public class ServiceOne extends Service{

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Started....",Toast.LENGTH_LONG).show();
        Thread thread = new Thread(new MyThreadClass(startId));
        thread.start();

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Stopped....",Toast.LENGTH_LONG).show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //Create a separate Thread
    final class MyThreadClass implements Runnable{
        int serviceId;

        public MyThreadClass(int serviceId) {
            this.serviceId = serviceId;
        }

        @Override
        public void run() {
            int i = 0;
            //synchronized means that only one thread at a time should be able to access
            synchronized (this){
                while (i<10) {
                    try {
                        wait(1500);
                        i++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                stopSelf();
            }
        }
    }
}
