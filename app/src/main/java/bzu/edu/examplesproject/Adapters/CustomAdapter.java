package bzu.edu.examplesproject.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import bzu.edu.examplesproject.CustomListActivity;
import bzu.edu.examplesproject.R;

/**
 * Created by mrashid on 5/8/2016.
 */
public class CustomAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    int [] imageId;
    String [] progNames;
    String [] prgmDescriptions;
    public CustomAdapter(CustomListActivity mainActivity, String[] prgmNameList, int[] prgmImages, String[] descriptions) {
        // TODO Auto-generated constructor stub
        progNames = prgmNameList;
        prgmDescriptions = descriptions;
        context = mainActivity;
        imageId = prgmImages;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return progNames.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class Holder
    {
        TextView tv;
        TextView txtvDescription;
        ImageView img;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.row_program_list, null);
        holder.tv = (TextView) rowView.findViewById(R.id.txtComputerLangName);
        holder.txtvDescription = (TextView) rowView.findViewById(R.id.txtDescription);
        holder.img = (ImageView) rowView.findViewById(R.id.imgvComputerLangIcon);

        holder.tv.setText(progNames[i]);
        holder.txtvDescription.setText(prgmDescriptions[i]);
        holder.img.setImageResource(imageId[i]);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "You Clicked "+ progNames[i], Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }
}
