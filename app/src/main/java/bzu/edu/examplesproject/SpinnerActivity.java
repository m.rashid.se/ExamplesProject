package bzu.edu.examplesproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SpinnerActivity extends AppCompatActivity {

    int first_spinner = 0, first_spinner_counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        //***Step 1
        Spinner spinner = (Spinner) findViewById(R.id.spnrPlanetsSpinner);

        //***Step 2
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,   //context for the activity
                R.array.planets_array,  //string array via resource file, which is going to load in spinner
                android.R.layout.simple_spinner_item    //layout for items of the spinner,
                );

        //***Step 3
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //***Step 4
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);


        //**** event Handling for spinner
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (first_spinner_counter < first_spinner) {
                    first_spinner_counter++;
                }else{
                    // An item was selected. You can retrieve the selected item using
                    parent.getItemAtPosition(pos);
                    TextView txt = (TextView)view;
                    txt.getText();
                    System.out.println(parent.getItemAtPosition(pos).toString());

                    Toast.makeText(parent.getContext(),"On Item Selected: You select "+txt.getText(),Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(),"On Nothing Selected: "+adapterView.toString(),Toast.LENGTH_LONG).show();
            }
        });
    }
}
