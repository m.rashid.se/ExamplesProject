package bzu.edu.examplesproject;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import bzu.edu.examplesproject.AsyncTasks.MyTaskOne;

public class AsyncTaskActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);

        final TextView txtvNotification = (TextView)findViewById(R.id.txtvNotification);
        final Button btnStartDownload = (Button)findViewById(R.id.btnStartDownload);
        btnStartDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyTaskOne myTaskOne = new MyTaskOne(AsyncTaskActivity.this,txtvNotification,btnStartDownload);
                myTaskOne.execute();
                btnStartDownload.setEnabled(false);
            }
        });
    }
}
