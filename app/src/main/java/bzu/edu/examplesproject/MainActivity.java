package bzu.edu.examplesproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // simple Button 1
        Button btnShowSimpleLv = (Button) findViewById(R.id.btnShowSimpleLv);
        Button btnShowComplexLv = (Button) findViewById(R.id.btnShowComplexLv);
        Button btnShowSpinner = (Button) findViewById(R.id.btnShowSpinner);
        Button btnShowAsyncActivity = (Button) findViewById(R.id.btnShowAsyncActivity);
        Button btnShowWebRequestTask = (Button) findViewById(R.id.btnShowWebRequestTask);
        Button btnShowServiceActivity = (Button) findViewById(R.id.btnShowServiceActivity);
        btnShowSimpleLv.setOnClickListener(onClickListener);
        btnShowSimpleLv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SimpleListViewActivity.class);
//                intent.setClassName("bzu.edu.examplesproject","bzu.edu.examplesproject.SimpleListViewActivity");
                intent.putExtra("whichList", "AndroidItem"); //Optional parameters
                startActivity(intent);
            }
        });

        // simple Button 2
        Button btnShowSimpleLv2 = (Button) findViewById(R.id.btnShowSimpleLv2);
        btnShowSimpleLv2.setOnClickListener(onClickListener);
        btnShowComplexLv.setOnClickListener(onClickListener);
        btnShowSpinner.setOnClickListener(onClickListener);
        btnShowAsyncActivity.setOnClickListener(onClickListener);
        btnShowWebRequestTask.setOnClickListener(onClickListener);
        btnShowServiceActivity.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            switch (view.getId()){
                case R.id.btnShowSimpleLv:
//                    Intent intent = new Intent(getApplicationContext(),SimpleListViewActivity.class);
                    intent.setClassName("bzu.edu.examplesproject","bzu.edu.examplesproject.SimpleListViewActivity");
                    intent.putExtra("whichList", "AndroidItem"); //Optional parameters
                    startActivity(intent);
                    break;
                case R.id.btnShowSimpleLv2:
//                    intent = new Intent(getApplicationContext(), SimpleListViewActivity.class);
                    intent.setClassName("bzu.edu.examplesproject","bzu.edu.examplesproject.SimpleListViewActivity");
                    intent.putExtra("whichList", "MyItem"); //Optional parameters
                    startActivity(intent);
                    break;

                case R.id.btnShowSpinner:
                    intent.setClassName("bzu.edu.examplesproject","bzu.edu.examplesproject.SpinnerActivity");
                    startActivity(intent);
                    break;

                case R.id.btnShowComplexLv:
                    intent.setClassName("bzu.edu.examplesproject","bzu.edu.examplesproject.CustomListActivity");
                    startActivity(intent);
                    break;

                case R.id.btnShowAsyncActivity:
                    intent.setClassName("bzu.edu.examplesproject","bzu.edu.examplesproject.AsyncTaskActivity");
                    startActivity(intent);
                    break;

                case R.id.btnShowWebRequestTask:
                    intent.setClassName("bzu.edu.examplesproject","bzu.edu.examplesproject.WebRequestActivity");
                    startActivity(intent);
                    break;

                case R.id.btnShowServiceActivity:
                    intent.setClassName("bzu.edu.examplesproject","bzu.edu.examplesproject.ServiceActivity");
                    startActivity(intent);
                    break;
            }
        }
    };
}
