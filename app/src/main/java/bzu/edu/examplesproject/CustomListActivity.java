package bzu.edu.examplesproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import bzu.edu.examplesproject.Adapters.CustomAdapter;

public class CustomListActivity extends AppCompatActivity {

    private ListView listView;
    public static int [] prgmImages = {R.drawable.java,
                                        R.drawable.jsp,
                                        R.drawable.android,
                                        R.drawable.c,
                                        R.drawable.cplus,
                                        R.drawable.csharp,
                                        R.drawable.objective_c,
                                        R.drawable.swift,
                                        R.drawable.asp,
                                        R.drawable.vb,
                                        R.drawable.python,
                                        R.drawable.php,
                                        R.drawable.ruby};
    public static String [] prgmNameList = {"JAVA",
                                            "JSP",
                                            "Android",
                                            "C Language",
                                            "C++ Language",
                                            "C# Language",
                                            "Objective C",
                                            "Swift Language",
                                            "ASP",
                                            "Visual Basic",
                                            "Python",
                                            "PHP",
                                            "Ruby"};

    public static String [] prgmDescriptionList = {"Platform Independent language",
            "Server side language",
            "Mobile language",
            "Low level language",
            "Object Oriented language",
            "Power full language by MicroSoft",
            "Apple Mobile language",
            "Swift Apple Mobile language",
            "Server side language",
            "Power full language by MicroSoft",
            "Server side language",
            "Server side language",
            "Server side language"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list);

        listView = (ListView) findViewById(R.id.lstvCompPrograms);
        listView.setAdapter(new CustomAdapter(CustomListActivity.this, prgmNameList,prgmImages,prgmDescriptionList));
    }
}
